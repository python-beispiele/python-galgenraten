galgen = """____
|  |
|    
|    
|    
|
----"""
elements = [(13, 'O'), (18, '/'), (19, '|'), (20, '\\'), (24, '/'), (26, '\\')]

def printGalgen(num):
    global galgen
    if num > 0:
        galgen = replaceIndex(galgen, elements[num-1][0], elements[num-1][1])
    print(galgen)
    print(f"{ersatz} {buchstaben} {fehlversuche}")

def replaceIndex(s, i, c):
    return s[:i] + c + s[i+1:]

wort = "haustier"
ersatz = "".ljust(len(wort),'_')
buchstaben = []

fehlversuche = 0
printGalgen(fehlversuche)
while fehlversuche < 6 and not ersatz == wort:
    eingabe = input("Rate einen Buchstaben: ")
    if eingabe in wort:
        index = [i for i, buchstabe in enumerate(wort) if buchstabe == eingabe]
        for i in index:
            ersatz = ersatz[:i] + eingabe + ersatz[i+1:]
    else:
        buchstaben.append(eingabe)
        fehlversuche += 1
    printGalgen(fehlversuche)

if ersatz == wort:
    print("Du hast gewonnen!")
else:
    print(f"Du hast verloren! Das gesuchte Wort war {wort}")