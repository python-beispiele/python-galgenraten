# Python Galgenraten

Das Spiel  Galgenraten

1. die Basisversion

```
________ [] 0
Rate einen Buchstaben: h
h_______ [] 0
Rate einen Buchstaben: a
ha______ [] 0
Rate einen Buchstaben: u
hau_____ [] 0
Rate einen Buchstaben: z
hau_____ ['z'] 1
Rate einen Buchstaben: j
hau_____ ['z', 'j'] 2
Rate einen Buchstaben: k
hau_____ ['z', 'j', 'k'] 3
Rate einen Buchstaben: l
hau_____ ['z', 'j', 'k', 'l'] 4
Rate einen Buchstaben: 
```

2. mit Grafik in der Konsole

```
____
|  |
|    
|    
|    
|
----
haus____ [] 0
Rate einen Buchstaben: t
____
|  |
|    
|    
|    
|
----
haust___ [] 0
Rate einen Buchstaben: z
____
|  |
|  O 
|    
|    
|
----
haust___ ['z'] 1
Rate einen Buchstaben: q
```

3. mit Zufälligem Wort aus Länderliste