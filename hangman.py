wort = "haustier"
ersatz = "".ljust(len(wort),'_')
buchstaben = []

fehlversuche = 0
print(f"{ersatz} {buchstaben} {fehlversuche}")
while fehlversuche < 6 and not ersatz == wort:
    eingabe = input("Rate einen Buchstaben: ")
    if eingabe in wort:
        index = [i for i, buchstabe in enumerate(wort) if buchstabe == eingabe]
        for i in index:
            ersatz = ersatz[:i] + eingabe + ersatz[i+1:]
    else:
        buchstaben.append(eingabe)
        fehlversuche += 1
    print(f"{ersatz} {buchstaben} {fehlversuche}")

if ersatz == wort:
    print("Du hast gewonnen!")
else:
    print(f"Du hast verloren! Das gesuchte Wort war {wort}")